package com.example.geolocalipro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button boton;
    Button boton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       boton= (Button) findViewById(R.id.btnactualizar);
       boton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent boton = new Intent(MainActivity.this, MapsActivity.class);
               startActivity(boton);
           }
       });

        boton1= (Button) findViewById(R.id.button5);
        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent boton1 = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(boton1);
            }
        });

    }
}
