package com.example.geolocalipro;

public class mapgeolocalizacion {

    private double latitud;
    private double longitud;


    public  mapgeolocalizacion(){

    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }


}
